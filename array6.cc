#include<cstring>
#include<iostream>
#include<cassert>
#define MAX_LEN 50
using namespace std;



string compress(string src)
{
    int charLen;
    char count[MAX_LEN] = {'\0'};
    int len = src.length();
    
    //if all characters are different then string size will be double
    
    char *dest = (char *)malloc(sizeof(char)*(len*2 + 1));
    
    int i, j = 0, k;
    
    /* traverse the input string one by one */
    for(i = 0; i < len; i++)
    {
        
        //copy new character
        dest[j++] = src[i];
        
        //count occurences of new character
        charLen = 1;
        while(i + 1 < len && src[i] == src[i+1])
        {
            charLen++;
            i++;
        }
        
        //convert int charLen to char
        sprintf(count, "%d", charLen);
        
        //copy count to compressed string
        for(k = 0; *(count+k); k++, j++)
        {
            dest[j] = count[k];
        }
    }
    
    //termination of string
    dest[j] = '\0';
    
    if(j>len)
        return src;
    else
        return dest;
}
bool compare(string str,string strR)
{
    if(str.compare(strR)==0)
        return true;
    else
        return false;
}

void test()
{   assert(compare(compress("abcaaabbb"),"abcaaabbb"));
    assert(!compare(compress("abcaaabbb"),"a1b1c1a3b3"));
    assert(compare(compress("abcd"),"abcd"));
    assert(!compare(compress("abcd"),"a1b1c1d1"));
    assert(compare(compress("aaabaaaaccaaaaba"),"a3b1a4c2a4b1a1"));
    assert(!compare(compress("aaabaaaaccaaaaba"),"aaabaaaaccaaaaba"));
    cout<<"All test cases passed"<<endl;
}
int main()
{
    if(1)
        test();
    else
        cout<<"Fail";
return 0;

}
