#include<iostream>
#include<cstring>
#include<cassert>

using namespace std;

bool editDistance(string s1, string s2)
{
    //lengths of given strings
    int m = s1.length(), n = s2.length();
 
    //if lengths differ by more than 1 edit distance isnt 1
    if (abs(m - n) > 1)
        return false;
 
    int count = 0; // Count of edits
 
    int i = 0, j = 0;
    while (i < m && j < n)
    {
        
        if (s1[i] != s2[j])				//current characters dont match
        {
            if (count == 1)				//more than 1 character dont match
                return false;
 
            
            if (m > n)					//string 1 is greater in length
                i++;
            else if (m< n)				//string 2 is greater in length
                j++;
            else 					//both strings are same length
            {
                i++;
                j++;
            }
             
            	
            count++;					//increment no of edits
        }
 
        else 						//if current characters match
        {
            i++;
            j++;
        }
    }
 
    
    if (i < m || j < n)					//last character extra in any string
        count++;
 
    return count == 1;
}


void test()
{
    assert(editDistance("abcd","abc"));
    assert(!editDistance("abcd",""));
    assert(editDistance("peek","peel"));
    assert(!editDistance("peak","peel"));
    assert(editDistance("abcd","abcde"));
     assert(!editDistance("abc","abcde"));
    cout<<"All test cases passed"<<endl;
}
int main()
{
   
   if(1)
       test();
    else
        cout<<"FAIL";
   return 0;
}
