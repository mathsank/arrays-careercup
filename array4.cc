#include<string>
#include<iostream>
#include<cassert>
using namespace std;

#define NO_OF_CHAR 256
  

bool permutationPalindrome(string str)
{
    
    int count[NO_OF_CHAR] = {0};
    if(str.empty())
        return false;
    // count no of occurences of each charater in string
    for (int i = 0; str[i];  i++)
        if((str[i]>=65 && str[i]<=90)|| (str[i]>=97 && str[i]<=122) || (str[i]>=48 && str[i]<=57))
	count[str[i]]++;
  
    // Count odd occurring characters
    int odd = 0;							//palindrome can have 1 odd occurence or zero odd occurence
    for (int i = 0; i < NO_OF_CHAR; i++)
    {
        if (count[i] %2 ==1)
            odd++;
 
        if (odd > 1)
            return false;
    }
  
     
    return true;
}
  
void test()
{
    assert(permutationPalindrome("abdbd"));
    assert(!permutationPalindrome("abcd"));
    assert(!permutationPalindrome(""));
    assert(permutationPalindrome("122a1"));
    assert(permutationPalindrome("12313"));
    assert(!permutationPalindrome("123"));
    cout<<"All testcases passed"<<endl;
}

int main()
{
  if(1)
      test();
    else
        cout<<"fail";
  
  return 0;
}
