#include<iostream>
#include<cstring>
#include<cassert>
# define NO_OF_CHAR 256
using namespace std;

bool isPermutation(string str1, string str2)
{
    // counts number of times each character occurs in each string
    int count1[NO_OF_CHAR] = {0};
    int count2[NO_OF_CHAR] = {0};
    int i; 
    
    for (i = 0; str1[i]!='\0' && str2[i]!='\0';  i++)
    {
        count1[str1[i]]++;
        count2[str2[i]]++;
    }
 
    // if strings are not of same length
    if (str1.length()!=str2.length())
      return false;
 
    // Compare count of characters
    for (i = 0; i < NO_OF_CHAR; i++)
        if (count1[i] != count2[i])
            return false;
 
    return true;
}

void test()
{
    assert(isPermutation("abcd","bcda"));
    assert(isPermutation("abcdbcda","aabbccdd"));
    assert(!isPermutation("abcd","bcd"));
    assert(!isPermutation("abcd",""));
    cout<<endl<<"All test cases passed\n";
}

int main()
{

  

	if(1)
        test();
    else
        cout<<"FAil";
 
    return 0;
}
