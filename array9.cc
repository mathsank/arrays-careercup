#include <iostream>
#include <string.h>
#include<cassert>
using namespace std;
 

bool areRotations(string str1, string str2)
{
  int size1   = str1.length();
  int size2   = str2.length();
  string temp;
  
 
  //if sizes are unequal not a substring
  if (size1 != size2)
     return false;
 
  //temp has string1 str twice so rotations will be covered in that
    temp[0] = '\0';
    temp = str1 + str1;
  
 
  //find returns -1 if substring doesnt exist
  if (temp.find(str2)!=-1)
    return true;
  else
    return false;
}
void test()
{
    assert(areRotations("abcd","bcda"));
    assert(areRotations("aaaa","aaaa"));
    assert(!areRotations("abcd","bcdaa"));
    assert(!areRotations("abcd","bcdd"));
    assert(!areRotations("","bcda"));
    assert(areRotations("",""));
    cout<<"All test cases passed"<<endl;
}

int main()
{
    if(1)
        test();
 
    
    return 0;
}
