#include<iostream>
#include<cassert>
using namespace std;
#define N 4

void rotateMatrix(int mat[][N])
{
    // Consider all squares one by one
    for (int x = 0; x < N / 2; x++)
    {
        // Consider elements in group of 4 in 
        // current square
        for (int y = x; y < N-x-1; y++)
        {
            // store current cell in temp variable - top
            int temp = mat[x][y];
            
            // move values from left to top
            mat[x][y] = mat[N-1-y][x];
            
            // move values from bottom to left
            mat[N-1-y][x] = mat[N-1-x][N-1-y];
            
            // move values from right to bottom
            mat[N-1-x][N-1-y] = mat[y][N-1-x];
            
            // assign temp to right - top
            mat[y][N-1-x] = temp;
            
        }
    }
}

void displayMatrix(int mat[N][N])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
            cout<<mat[i][j]<<" ";
        
        cout<<"\n";
    }
     cout<<"\n";
}

bool compareMatrix(int mat[][N], int matR[][N])
{
    for(int i=0;i<N;i++)
        for(int j=0;j<N;j++)
            if(mat[i][j]!=matR[i][j])
                return false;
    return true;
}

void test()
{
    int mat[N][N] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    int matR[N][N] = {13,9,5,1,14,10,6,2,15,11,7,3,16,12,8,4};
    assert(!compareMatrix(mat,matR));
    rotateMatrix(mat);
    assert(compareMatrix(mat,matR));
    
    cout<<"Test case passed"<<endl;
}
int main()
{   if(1)
        test();
    
    return 0;
}
