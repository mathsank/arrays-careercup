#include<iostream>
#include<cstring>
#include<cassert>
#define MAX 1000
using namespace std;

string replaceSpaces(string str, int len)
{
   int spaceCount=0,newLen,i=0;
	for(i=0;i<len;i++)
	{ if(str[i]==' ')
		spaceCount++;
	}

    while (str[i-1] == ' ')
    {
       spaceCount--;
       i--;
    }
    newLen = i + spaceCount*2 +1;           //new length 
    
    //start replacing spaces from end
    int index = newLen-1;
    str.resize(index); 
    //add eos character
    str[index--] = '\0';
 
    //fill string from end
    for (int j=i-1; j>=0; j--)
    {
        // inserts %20 in place of space
        if (str[j] == ' ')
        {
            str[index] = '0';
            str[index - 1] = '2';
            str[index - 2] = '%';
            index = index - 3;
        }
        else
        {
            str[index] = str[j];
            index--;
        }
    }
//cout<<endl<<str<<endl;
return str;
}

bool compare(string str,string strR)
{
    if(str.compare(strR)==0)
        return true;
    else
        return false;
}

void test()
{
    assert(compare(replaceSpaces("ab cd   ",8),"ab%20cd"));
    assert(compare(replaceSpaces("ab cd",5),"ab%20cd"));
    assert(compare(replaceSpaces("    a",5),"%20%20%20%20a"));
    assert(compare(replaceSpaces("a    ",5),"a"));
    assert(compare(replaceSpaces("    ",4),""));
    cout<<"All test cases passed"<<endl;
}

int main()
{
    	if(1)
            test();
}
