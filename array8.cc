#include <iostream>
#include<cassert>
using namespace std;
#define R 3
#define C 3
 
void modifyMatrix(int mat[R][C])
{
    bool row[R];                            //row and col arrays iterate through matrix to find rows/cols that are 0
    bool col[C];
 
    int i, j;
 
 
    //initially row and col values are 0
    for (i = 0; i < R; i++)
    {
       row[i] = 0;
    }
 
 
    
    for (i = 0; i < C; i++)
    {
       col[i] = 0;
    }
 
 
    //rows and cols with 0 are flagged as 1
    for (i = 0; i < R; i++)
    {
        for (j = 0; j < C; j++)
        {
            if (mat[i][j] == 0)
            {
                row[i] = 1;
                col[j] = 1;
            }
        }
    }
 
    //matrix modified
    for (i = 0; i < R; i++)
    {
        for (j = 0; j < C; j++)
        {
            if ( row[i] == 1 || col[j] == 1 )
            {
                mat[i][j] = 0;
            }
        }
    }
}
 
//prints matrix
void printMatrix(int mat[R][C])
{
    int i, j;
    for (i = 0; i < R; i++)
    {
        for (j = 0; j < C; j++)
        {
            cout<<mat[i][j];
        }
        cout<<endl;
    }
}

bool compareMatrix(int mat[][C], int matR[][C])
{
    for(int i=0;i<R;i++)
        for(int j=0;j<C;j++)
            if(mat[i][j]!=matR[i][j])
                return false;
    return true;
}

void test()
{   //test case 1
    int mat1[R][C] = {1,0,1,0,1,1,1,1,0};
    int mat1Z[R][C] = {0,0,0,0,0,0,0,0,0};
    assert(!compareMatrix(mat1,mat1Z));
    modifyMatrix(mat1);
    assert(compareMatrix(mat1,mat1Z));
    
    //test case 2
    int mat2[R][C] = {1,1,1,0,1,1,1,1,0};
    int mat2Z[R][C] = {0,1,0,0,0,0,0,0,0};
    assert(!compareMatrix(mat2,mat2Z));
    modifyMatrix(mat2);
    assert(compareMatrix(mat2,mat2Z));
        
    //test case 3
    int mat3[R][C] = {1,1,1,1,1,1,1,1,0};
    int mat3Z[R][C] = {1,1,0,1,1,0,0,0,0};
    assert(!compareMatrix(mat3,mat3Z));
    modifyMatrix(mat3);
    assert(compareMatrix(mat3,mat3Z));
    
    cout<<"All test cases passed"<<endl;
    
}
int main()
{
    
    
    if(1)
        test();
    
    return 0;
}
