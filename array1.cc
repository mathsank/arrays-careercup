#include<iostream>
#include <cstring>
#include <cassert>
using namespace std;
 
const int CHARMAX = 256;
 
bool uniqueString(string str)
{
    
    if (str.length() > CHARMAX)					//if length is greater than 265 characters must be repeated
        return false;
 
    bool chars[CHARMAX] = {0};					//each index is for each character. if it is true then character exists in string
    for (int i = 0; i < str.length(); i++) 
    {
	int val = str.at(i);
        if (chars[val] == true) 			//checking if it is true to see if character already exists
            return false;
         
        chars[val] = true;				//if it is not true then character encountered for the 1st time so make it true
    }
    return true;
}


bool uniqueS(string str)
{

	for(int i = 0; i < str.length(); i++) 			//check each character with every other character
	{
        for(int j = i + 1; j < str.length(); j++)
	{
            if(str[i] == str[j])
	    {
                return false;
            }
	}
	}




	return true;						//no duplicates


} 
void test()
{
    assert(uniqueString("abcd"));
    assert(uniqueS("abcd"));
    assert(!uniqueString("abcda"));
    assert(!uniqueS("abcda"));

    cout<<endl<<"All test cases passed\n";
}



int main()
{   
    if(1)
        test();
    else
        cout<<"Fail";
    
    return 0;
}
